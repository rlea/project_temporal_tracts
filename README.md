# project_temporal_tracts

This repository contains processing code and result files associated with the following publication: 

## Longitudinal connections and the organization of the temporal cortex in macaques, great apes, and humans

### Roumazeilles L, Eichert N, Bryant KL, Folloni D, Sallet J, Vijayakumar S, Foxley S, Tendler BC, Jbabdi S, Reveley C, Verhagen L, Dershowitz LB, Guthrie M, Flach E, Miller KL, Mars RB.

Explanations to use the scripts used for the results in the paper can be found in /scripts/scripts_README.md
Some scripts call other scripts from MrCat toolbox available at: https://github.com/neuroecology/MrCat

## Data folder organisation

In each script edit, the ```rootDir``` with your path to the data, masks and results.
The data must be organised as follow: species_directory>subjects_directory>files.

Species could be: macaque, human, chimpIV, chimpPM, gorilla.

Subjects are named: subject_01, subject_02 (ect).

data must contain: 
- the bedpostX directory as dMRI.bedpostX for each subject

- the standard image used in the species subfolder (different resolution for different steps and need one brain exctracted)

- warps from standard space to diffusion space and inversly

- grey matter stop masks

- temporal surface mask for humans and macaques

- surface files for macaque (one standard, midthickness) and humans (one for each subject midthickness)

- human_macaque combined surface file


