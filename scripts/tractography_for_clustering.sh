#!/bin/bash

#### ---------------
# edit with your own path
rootDir=/mypath/
#### ---------------

# Loop trough species, hemispheres, ROI sections

species='macaque human chimpIV chimpPM gorilla'
# possibilities: macaque human chimpIV chimpPM gorilla

sections='ant mid post'
# possibilities: ant mid post

for s in $species; do # loop trough species
	# define for each species the hemispheres to use, the standard space and the subjects name
	if [ "$s" = "macaque" ]; then
		hemis='L R'
		std=$rootDir/data/$s/F99_05mm_brain
		subjects='01 02 03 04'
	elif [ "$s" = "human" ]; then
		hemis='L R'
		std=$rootDir/data/$s/MNI152_T1_1mm_brain
		subjects='01 02 03 04 05 06 07 08 09 10'
	elif [ "$s" = "chimpIV" ]; then
		hemis='L R'
		std=$rootDir/data/$s/ChimpYerkes29_T1w_08mm_brain
		subjects='01 02 03'
	elif [ "$s" = "chimpPM" ]; then 
		hemis='R' # only the right hemispehre is available
		# only one individual so stay in individual space
		subjects='01'
	elif [ "$s" = "gorilla" ]; then
		hemis='R' # only the right hemispehre is available
		# only one individual so stay in individual space
		subjects='01'
	fi

	for subj in $subjects;do # loop trough subjects
		bedpostDir=$rootDir/data/$s/subject_${subj}/dMRI.bedpostX #bedpost directory
		resultsDir=$rootDir/results/clustering/$s/subject_${subj} #results directory
		binary_mask=$bedpostDir/nodif_brain_mask #binary mask defined as the no diffusion mask from bedpost output
		target_downsampled='$bedpostDir/nodif_brain_mask_down' #a previously downsampled nodif mask

		for h in $hemis;do # loop trough hemispheres
			for sec in $sections; do # loop trough sections
				echo "Doing $s subject_${subj} ROI_${h}_${sec}"
				clustering_ROI=$rootDir/masks/clustering_ROIs/$s/ROI_${h}_${sec}

				if [ "$s" = "chimpPM" ] || [ "$s" = "gorilla" ]; then
					# run probtrackx2 from FSL with omatrix2 option
					probtrackx2 -x $clustering_ROI \
						-m $binary_mask \
						--samples=$bedpostDir/cudimot_Param/merged \
						--opd \
						--loopcheck \
						--forcedir \
						--dir=$resultsDir/"outputMat_${h}_${sec}" \
						--omatrix2 \
						--target2=$target_downsampled

				else
					std2dMRI=$rootDir/data/$s/subject_${subj}/standard_to_dMRI_warp #warp from standard space to diffusion space
					dMRI2std=$rootDir/data/$s/subject_${subj}/dMRI_to_standard_warp #warp from diffusion space to standard space

					# run probtrackx2 from FSL with omatrix2 option
					probtrackx2 -x $clustering_ROI \
						-m $binary_mask \
						--samples=$bedpostDir/merged \
						--xfm=$std2dMRI --invxfm=$dMRI2std \
						--seedref=$std \
						--opd \
						--loopcheck \
						--forcedir \
						--dir=$resultsDir/"outputMat_${h}_${sec}" \
						--omatrix2 \
						--target2=$target_downsampled
						
				fi
			done # loop sections done
		done # loop hemimspheres done

	done # loop subjects done

done # loop species done
