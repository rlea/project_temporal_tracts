%% Average fdt_matrix2

%% ----------------
% edit with your own path
rootDir = '/mypath/';
%% ----------------


%% Loop trough species, hemispheres
species = {'macaque', 'human'};
% possibilities: 'macaque', 'human'
hemis = {'l', 'r'};

for s = 1:length(species) % Loop trough species
    specie = species{s};
    resultsDir = [rootDir '/results/blueprint/' specie];
    switch specie % define the subjects for each species
        case 'macaque'
            subjects = {'01','02','03','04'};
        case 'human'
            subjects = {'01','02','03','04','05','06','07','08','09','10'};
    end
    for h = 1:length(hemis) % Loop trough hemis
        hemi = hemis{h};
        avgmat=0;
        for subj = 1:length(subjects) % loop trough subjects
            subject = subjects{subj};
            disp(['-------' subject '--------'])
            fname=[resultsDir '/subjects' subject '/surfseed_' hemi '/fdt_matrix2.dot'];
            mat=spconvert(load(fname));
            avgmat = avgmat+mat;
            
        end %subj
        avgmat=avgmat/length(subjects);
        save([resultsDir '/AVG_Matrix_cortexbrain_' hemi '.mat'],'avgmat','-v7.3');
        
    end %hemi
end % species